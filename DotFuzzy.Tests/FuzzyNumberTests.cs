﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DotFuzzy.Tests
{
    [TestClass]
    public class FuzzyNumberTests
    {

        [TestMethod]
        public void GetAlphaCut()
        {
            var factory = new FuzzyNumberFactory();
            var number = factory.CreateTFN(1, 2, 3);

            Assert.AreEqual(new Interval(2), number.GetAlphaCut(1));
            Assert.AreEqual(new Interval(1.5, 2.5), number.GetAlphaCut(0.5));

            number = factory.CreateTFN(0, 2, 6);
            Assert.AreEqual(new Interval(1.6, 2.8), number.GetAlphaCut(0.8));
        }

        [TestMethod]
        public void GetMembership()
        {
            var factory = new FuzzyNumberFactory();
            var number = factory.CreateTFN(1, 2, 10);

            Assert.AreEqual(0, number.GetMembership(1));
            Assert.AreEqual(0.25, number.GetMembership(8));
            Assert.AreEqual(1, number.GetMembership(2));

            number = factory.CreateTFN(1, 2, 3);

            Assert.AreEqual(0.1, number.GetMembership(1.1));
            Assert.AreEqual(0.8, number.GetMembership(1.8));
        }

        [TestMethod]
        public void Summation()
        {
            var factory = new FuzzyNumberFactory();

            Assert.AreEqual(factory.CreateTFN(1, 3, 5),
                factory.CreateTFN(0, 1, 2) + factory.CreateTFN(1, 2, 3));
        }

        [TestMethod]
        public void Substraction()
        {
            var factory = new FuzzyNumberFactory();

            Assert.AreEqual(factory.CreateTFN(-4, -2, 0),
                factory.CreateTFN(0, 1, 2) - factory.CreateTFN(2, 3, 4));
        }

        [TestMethod]
        public void Multiplication()
        {
            var factory = new FuzzyNumberFactory(2);

            Assert.AreEqual(factory.CreateTFN(4, 10, 18),
                factory.CreateTFN(1, 2, 3) * factory.CreateTFN(4, 5, 6));
        }

        [TestMethod]
        public void Division()
        {
            var factory = new FuzzyNumberFactory(2);

            Assert.AreEqual(factory.CreateTFN(1, 2, 3),
                factory.CreateTFN(1, 2, 3) / factory.CreateTFN(1, 1, 1));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void DivisionByZero()
        {
            var factory = new FuzzyNumberFactory();
            var number = 3 / factory.CreateTFN(-1, 2, 4);
        }

    }
}
