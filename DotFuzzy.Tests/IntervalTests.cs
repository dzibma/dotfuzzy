﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DotFuzzy.Tests
{
    [TestClass]
    public class IntervalTests
    {

        [TestMethod]
        public void Containts()
        {
            Interval ival = new Interval(-2, 2);

            Assert.IsTrue(ival.Contains(0));
            Assert.IsTrue(ival.Contains(new Interval(-2, 1)));
            Assert.IsFalse(ival.Contains(2.001));
            Assert.IsFalse(ival.Contains(new Interval(0, 3)));
        }

        [TestMethod]
        public void IntersectsWith()
        {
            Interval ival = new Interval(0, 2);

            Assert.IsTrue(ival.IntersectsWith(new Interval(1, 3)));
            Assert.IsTrue(ival.IntersectsWith(new Interval(-2, 0)));
            Assert.IsTrue(ival.IntersectsWith(new Interval(-3, 3)));
            Assert.IsFalse(ival.IntersectsWith(new Interval(-0.001)));
        }

        [TestMethod]
        public void Summation()
        {
            Assert.AreEqual(
                new Interval(1.1, 25), 
                new Interval(.1, 23) + new Interval(1, 2));

            Assert.AreEqual(
                new Interval(-22.5, 23.5), 
                new Interval(-23, 23) + .5);

            Assert.AreEqual(
                new Interval(-26.333, -23),
                - 23 + new Interval(-3.333, 0));
        }

        [TestMethod]
        public void Substraction()
        {
            Assert.AreEqual(
                new Interval(-1.9, 22), 
                new Interval(.1, 23) - new Interval(1, 2));

            Assert.AreEqual(
                new Interval(-23.5, 22.5), 
                new Interval(-23, 23) - .5);

            Assert.AreEqual(
                new Interval(-23, -19.667),
                - 23 - new Interval(-3.333, 0));
        }

        [TestMethod]
        public void Multiplication()
        {
            Assert.AreEqual(
                new Interval(1, 46), 
                new Interval(1, 23) * new Interval(1, 2));

            Assert.AreEqual(
                new Interval(-12, 12), 
                2 * new Interval(-2, 2) * new Interval(2, 3));

            Assert.AreEqual(
                new Interval(-6, 6), 
                new Interval(-2, 2) * new Interval(2, 3) * -1);

            Assert.AreEqual(
                new Interval(-2, 1), 
                new Interval(0, 1) * new Interval(-2, 1));
        }

        [TestMethod]
        public void Division()
        {
            Assert.AreEqual(
                new Interval(0.5, 23), 
                new Interval(1, 23) / new Interval(1, 2));

            Assert.AreEqual(
                new Interval(-1, 1), 
                new Interval(-2, 2) / new Interval(2, 3));

            Assert.AreEqual(
                new Interval(1, 2), 
                8 / new Interval(2, 4) / 2);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void DivisionByZero()
        {
            var ival = new Interval(3) / new Interval(-1, 1);
        }

    }
}
