﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DotFuzzy.Tests
{
    [TestClass]
    public class FunctionsTests
    {
         [TestMethod]
        public void Atan2()
        {
             var factory = new FuzzyNumberFactory(2);

             var y = factory.CreateTFN(-2, 3, 5);
             var x = factory.CreateTFN(-1.5, -1, -0.5);

             Assert.AreEqual(
                 factory.CreateTFN(-4.6127, -4.3906, -1.8158), 
                 Functions.Atan2(y, x));

             y = factory.CreateTFN(-2, 3, 5);
             x = factory.CreateTFN(-4.8, -4, 1.5);

             Assert.AreEqual(
                 factory.CreateTFN(-Math.PI, Math.Atan2(y.Top.A, x.Top.A), Math.PI),
                 Functions.Atan2(y, x));
        }
    }
}
