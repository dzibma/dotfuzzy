﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DotFuzzy.Tests
{
    [TestClass]
    public class IntervalFunctionsTests
    {
        [TestMethod]
        public void Sin()
        {
            Assert.AreEqual(
                new Interval(-1, 1),
                Functions.Sin(new Interval(0, 2 * Math.PI)));

            Assert.AreEqual(
                new Interval(0, 0.8415),
                Functions.Sin(new Interval(0, 1)));
        }

        [TestMethod]
        public void Cos()
        {
            Assert.AreEqual(
                new Interval(-1, 1),
                Functions.Cos(new Interval(0, 2 * Math.PI)));

            Assert.AreEqual(
                new Interval(0.5403, 1),
                Functions.Cos(new Interval(0, 1)));
        }
    }
}
