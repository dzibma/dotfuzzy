﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotFuzzy
{
    public static class Statistics
    {

        private static double intersectionDegree(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4)
        {
            double x12 = x1 - x2;
            double x34 = x3 - x4;
            double y12 = y1 - y2;
            double y34 = y3 - y4;

            double c = x12 * y34 - y12 * x34;


            double a = x1 * y2 - y1 * x2;
            double b = x3 * y4 - y3 * x4;

            return (a * y34 - b * y12) / c;
        }

        public static double possibilityOfExceedance(FuzzyNumber x, FuzzyNumber y)
        {
            var count = x.AlphaCuts.Count;
            if (count != y.AlphaCuts.Count)
                throw new NotImplementedException();

            if (x.Top.B >= y.Top.A)
                return 1;

            for (var i = count - 1; i >= 0; i--)
            {
                if (x.AlphaCuts[i].B >= y.AlphaCuts[i].A)
                {
                    var y1 = i / (double)(count - 1);
                    var y2 = (i + 1) / (double)(count - 1);

                    return intersectionDegree(
                            x.AlphaCuts[i].B, y1,
                            x.AlphaCuts[i + 1].B, y2,
                            y.AlphaCuts[i].A, y1,
                            y.AlphaCuts[i + 1].A, y2
                        );
                }
            }

            return 0;
        }

        public static double necessityOfExceedance(FuzzyNumber x, FuzzyNumber y)
        {
            var count = x.AlphaCuts.Count;
            if (count != y.AlphaCuts.Count)
                throw new NotImplementedException();

            if (x.Bottom.A >= y.Top.A)
                return 1;

            for (var i = count - 1; i >= 0; i--)
            {
                var inverse = count - i - 1;

                if (y.AlphaCuts[i].A <= x.AlphaCuts[inverse].A)
                {
                    var y1 = i / (double)(count - 1);
                    var y2 = (i + 1) / (double)(count - 1);

                    return intersectionDegree(
                            y.AlphaCuts[i].A, y1,
                            y.AlphaCuts[i + 1].A, y2,
                            x.AlphaCuts[inverse].A, y1,
                            x.AlphaCuts[inverse - 1].A, y2
                        );
                }
            }

            return 0;
        }

        public static double possibilityOfStrictExceedance(FuzzyNumber x, FuzzyNumber y)
        {
            var count = x.AlphaCuts.Count;
            if (count != y.AlphaCuts.Count)
                throw new NotImplementedException();

            if (y.Bottom.B <= x.Top.B)
                return 1;

            for (var i = count - 1; i >= 0; i--)
            {
                var inverse = count - i - 1;

                if (x.AlphaCuts[i].B > y.AlphaCuts[inverse].B)
                {
                    var y1 = i / (double)(count - 1);
                    var y2 = (i + 1) / (double)(count - 1);

                    return intersectionDegree(
                            y.AlphaCuts[inverse].B, y1,
                            y.AlphaCuts[inverse - 1].B, y2,
                            x.AlphaCuts[i].B, y1,
                            x.AlphaCuts[i + 1].B, y2
                        );
                }
            }

            return 0;
        }

        public static double necessityOfStrictExceedance(FuzzyNumber x, FuzzyNumber y)
        {
            var count = x.AlphaCuts.Count;
            if (count != y.AlphaCuts.Count)
                throw new NotImplementedException();

            if (y.Bottom.B <= x.Bottom.A)
                return 1;

            for (var i = count - 1; i >= 0; i--)
            {
                var inverse = count - i - 1;

                if (y.AlphaCuts[inverse].B < x.AlphaCuts[inverse].A)
                {
                    var y1 = i / (double)(count - 1);
                    var y2 = (i + 1) / (double)(count - 1);

                    return intersectionDegree(
                            x.AlphaCuts[inverse].A, y1,
                            x.AlphaCuts[inverse - 1].A, y2,
                            y.AlphaCuts[inverse].B, y1,
                            y.AlphaCuts[inverse - 1].B, y2
                        );
                }
            }

            return 0;
        }

        public static double possibilityOfUndervaluation(FuzzyNumber x, FuzzyNumber y)
        {
            var count = x.AlphaCuts.Count;
            if (count != y.AlphaCuts.Count)
                throw new NotImplementedException();

            if (x.Top.A <= y.Top.B)
                return 1;

            for (var i = count - 1; i >= 0; i--)
            { 
                if (x.AlphaCuts[i].A <= y.AlphaCuts[i].B)
                {
                    var y1 = i / (double)(count - 1);
                    var y2 = (i + 1) / (double)(count - 1);

                    return intersectionDegree(
                            x.AlphaCuts[i].A, y1,
                            x.AlphaCuts[i + 1].A, y2,
                            y.AlphaCuts[i].B, y1,
                            y.AlphaCuts[i + 1].B, y2
                        );
                }
            }

            return 0;
        }

        public static double necessityOfUndervaluation(FuzzyNumber x, FuzzyNumber y)
        {
            var count = x.AlphaCuts.Count;
            if (count != y.AlphaCuts.Count)
                throw new NotImplementedException();

            if (x.Bottom.B <= y.Top.B)
                return 1;

            for (var i = count - 1; i >= 0; i--)
            {
                var inverse = count - i - 1;

                if (y.AlphaCuts[i].B >= x.AlphaCuts[inverse].B)
                {
                    var y1 = i / (double)(count - 1);
                    var y2 = (i + 1) / (double)(count - 1);

                    return intersectionDegree(
                            y.AlphaCuts[i].B, y1,
                            y.AlphaCuts[i + 1].B, y2,
                            x.AlphaCuts[inverse].B, y1,
                            x.AlphaCuts[inverse - 1].B, y2
                        );
                }
            }

            return 0;
        }

        public static double possibilityOfStrictUndervaluation(FuzzyNumber x, FuzzyNumber y)
        {
            var count = x.AlphaCuts.Count;
            if (count != y.AlphaCuts.Count)
                throw new NotImplementedException();

            if (y.Bottom.A >= x.Top.A)
                return 1;

            for (var i = count - 1; i >= 0; i--)
            {
                var inverse = count - i - 1;

                if (y.AlphaCuts[inverse].A >= x.AlphaCuts[i].A)
                {
                    var y1 = i / (double)(count - 1);
                    var y2 = (i + 1) / (double)(count - 1);

                    return intersectionDegree(
                            x.AlphaCuts[i].A, y1,
                            x.AlphaCuts[i + 1].A, y2,
                            y.AlphaCuts[inverse].A, y1,
                            y.AlphaCuts[inverse - 1].A, y2
                        );
                }
            }

            return 0;
        }

        public static double necessityOfStrictUndervaluation(FuzzyNumber x, FuzzyNumber y)
        {
            var count = x.AlphaCuts.Count;
            if (count != y.AlphaCuts.Count)
                throw new NotImplementedException();

            if (y.Bottom.A >= x.Bottom.B)
                return 1;

            for (var i = count - 1; i >= 0; i--)
            {
                var inverse = count - i - 1;

                if (y.AlphaCuts[inverse].A >= x.AlphaCuts[inverse].B)
                {
                    var y1 = i / (double)(count - 1);
                    var y2 = (i + 1) / (double)(count - 1);

                    return intersectionDegree(
                            x.AlphaCuts[inverse].B, y1,
                            x.AlphaCuts[inverse - 1].B, y2,
                            y.AlphaCuts[inverse].A, y1,
                            y.AlphaCuts[inverse - 1].A, y2
                        );
                }
            }

            return 0;
        }

    }
}
