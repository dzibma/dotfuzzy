﻿using System;

namespace DotFuzzy
{
    public static partial class Functions
    {
        /// <summary>
        /// Inverse function of cosine
        /// </summary>
        /// <param name="interval"></param>
        /// <returns>Interval of angles in radians</returns>
        public static Interval Acos(Interval interval)
        {
            if (interval.A < -1 || interval.B > 1)
                throw new ArgumentOutOfRangeException(String.Format("Interval {0}", interval.ToString()), "Value outside the domain of a function.");

            return new Interval(Math.Asin(interval.A), Math.Asin(interval.B));
        }

        /// <summary>
        /// Inverse function of cosine
        /// </summary>
        /// <param name="number"></param>
        /// <returns>Fuzzy angle in radians</returns>
        public static FuzzyNumber Acos(FuzzyNumber number)
        {
            return FuzzyNumber.ExecuteUnary(number, x => Acos(x));
        }
    }
}
