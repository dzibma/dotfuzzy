﻿using System;

namespace DotFuzzy
{
    public static partial class Functions
    {
        /// <summary>
        /// Sine of the specified angle
        /// </summary>
        /// <param name="interval">Interval of angles in radians</param>
        /// <returns></returns>
        public static Interval Sin(Interval interval)
        {
            double a, b, critical;
            critical = Math.Floor((interval.A / Math.PI)) * Math.PI + Math.PI / 2;

            if (interval.Contains(critical))
                a = Math.Sin(critical);
            else
            {
                critical += Math.PI;

                if (interval.Contains(critical))
                    a = Math.Sin(critical);
                else
                {
                    a = Math.Sin(interval.A);
                    b = Math.Sin(interval.B);

                    return (b > a) ? new Interval(a, b) : new Interval(b, a);
                }
            }

            critical += Math.PI;

            if (interval.Contains(critical))
                b = Math.Sin(critical);
            else
            {
                b = (a > 0)
                    ? Math.Min(Math.Sin(interval.A), Math.Sin(interval.B))
                    : Math.Max(Math.Sin(interval.A), Math.Sin(interval.B));
            }

            return (b > a) ? new Interval(a, b) : new Interval(b, a);
        }

        /// <summary>
        /// Sine of the specified angle
        /// </summary>
        /// <param name="number">Angle in radians</param>
        /// <returns></returns>
        public static FuzzyNumber Sin(FuzzyNumber number)
        {
            return FuzzyNumber.ExecuteUnary(number, x => Sin(x));
        }

    }
}
