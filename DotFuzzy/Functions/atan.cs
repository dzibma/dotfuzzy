﻿using System;

namespace DotFuzzy
{
    public static partial class Functions
    {
        /// <summary>
        /// Inverse function of tangent
        /// </summary>
        /// <param name="interval"></param>
        /// <returns>Interval of angles in radians</returns>
        public static Interval Atan(Interval interval)
        {
            return new Interval(Math.Atan(interval.A), Math.Atan(interval.B));
        }

        /// <summary>
        /// Inverse function of tangent
        /// </summary>
        /// <param name="number"></param>
        /// <returns>Fuzzy angle in radians</returns>
        public static FuzzyNumber Atan(FuzzyNumber number)
        {
            return FuzzyNumber.ExecuteUnary(number, x => Atan(x));
        }
    }
}
