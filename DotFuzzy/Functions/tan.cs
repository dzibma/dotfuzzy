﻿using System;

namespace DotFuzzy
{
    public static partial class Functions
    {
        /// <summary>
        /// Tangent of the specified angle
        /// </summary>
        /// <param name="interval"></param>
        /// <returns></returns>
        public static Interval Tan(Interval interval)
        {
            double zeroPoint = Math.Floor((interval.A / Math.PI)) * Math.PI;
            double offset = Math.PI / 2;

            if (interval.Contains(zeroPoint - offset) || interval.Contains(zeroPoint + offset))
                throw new ArgumentOutOfRangeException(String.Format("Interval {0}", interval.ToString()), "Value outside the domain of a function.");

            return new Interval(Math.Tan(interval.A), Math.Tan(interval.B));
        }

        /// <summary>
        /// Tangent of the specified angle
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static FuzzyNumber Tan(FuzzyNumber number)
        {
            return FuzzyNumber.ExecuteUnary(number, x => Tan(x));
        }
    }
}
