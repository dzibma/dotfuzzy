﻿using System;

namespace DotFuzzy
{
    public static partial class Functions
    {
        /// <summary>
        /// Raises an interval to the specified power
        /// </summary>
        /// <param name="interval"></param>
        /// <param name="power"></param>
        /// <returns></returns>
        public static Interval Pow(Interval interval, double power)
        {
            if (power % 2 == 0)
            {
                if (interval.B < 0)
                    return new Interval(Math.Pow(interval.B, power), Math.Pow(interval.A, power));

                else if (interval.A < 0)
                    return new Interval(0, Math.Max(Math.Pow(interval.A, power), Math.Pow(interval.B, power)));
            }

            return new Interval(Math.Pow(interval.A, power), Math.Pow(interval.B, power));
        }

        /// <summary>
        /// Raises a Fuzzy number to a specified power
        /// </summary>
        /// <param name="number"></param>
        /// <param name="power"></param>
        /// <returns></returns>
        public static FuzzyNumber Pow(FuzzyNumber number, double power)
        {
            return FuzzyNumber.ExecuteUnary(number, x => Pow(x, power));
        }
    }
}
