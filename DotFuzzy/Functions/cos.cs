﻿using System;

namespace DotFuzzy
{
    public static partial class Functions
    {
        /// <summary>
        /// Cosine of the specified angle
        /// </summary>
        /// <param name="interval">Interval of angles in radians</param>
        /// <returns></returns>
        public static Interval Cos(Interval interval)
        {
            double a, b, critical;
            critical = Math.Floor((interval.A / Math.PI)) * Math.PI;

            if (interval.Contains(critical))
                a = Math.Cos(critical);
            else
            {
                critical += Math.PI;

                if (interval.Contains(critical))
                    a = Math.Cos(critical);
                else
                {
                    a = Math.Cos(interval.A);
                    b = Math.Cos(interval.B);

                    return (b > a) ? new Interval(a, b) : new Interval(b, a);
                }
            }

            critical += Math.PI;

            if (interval.Contains(critical))
                b = Math.Cos(critical);
            else
            {
                b = (a > 0)
                    ? Math.Min(Math.Cos(interval.A), Math.Cos(interval.B))
                    : Math.Max(Math.Cos(interval.A), Math.Cos(interval.B));
            }

            return (b > a) ? new Interval(a, b) : new Interval(b, a);
        }

        /// <summary>
        /// Cosine of the specified angle
        /// </summary>
        /// <param name="number">Angle in radians</param>
        /// <returns></returns>
        public static FuzzyNumber Cos(FuzzyNumber number)
        {
            return FuzzyNumber.ExecuteUnary(number, x => Cos(x));
        }
    }
}
