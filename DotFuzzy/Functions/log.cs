﻿using System;

namespace DotFuzzy
{
    public static partial class Functions
    {
        /// <summary>
        /// Logarithm of a specified interval in a specified base
        /// </summary>
        /// <param name="interval"></param>
        /// <param name="newBase"></param>
        /// <returns></returns>
        public static Interval Log(Interval interval, double newBase)
        {
            if (newBase <= 0 || newBase == 1)
                throw new ArgumentOutOfRangeException("Logarithm Base");

            if (interval.A <= 0)
                throw new ArgumentOutOfRangeException(String.Format("Interval {0}", interval.ToString()), "Value outside the domain of a function.");

            return new Interval(Math.Log(interval.A, newBase), Math.Log(interval.B, newBase));
        }

        /// <summary>
        /// Base e logarithm of a specified interval
        /// </summary>
        /// <param name="interval"></param>
        /// <returns></returns>
        public static Interval Log(Interval interval)
        {
            return Log(interval, Math.E);
        }

        /// <summary>
        /// Logarithm of a specified fuzzy number in a specified base
        /// </summary>
        /// <param name="number"></param>
        /// <param name="newBase"></param>
        /// <returns></returns>
        public static FuzzyNumber Log(FuzzyNumber number, double newBase)
        {
            return FuzzyNumber.ExecuteUnary(number, x => Log(x, newBase));
        }

        /// <summary>
        /// Base e logarithm of a specified fuzzy number
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static FuzzyNumber Log(FuzzyNumber number)
        {
            return Log(number, Math.E);
        }
    }
}
