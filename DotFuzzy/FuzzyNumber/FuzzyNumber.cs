﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace DotFuzzy
{
    /// <summary>
    /// Piecewise linear Fuzzy number
    /// </summary>
    public partial class FuzzyNumber : IEquatable<FuzzyNumber>
    {

        private Interval[] alphaCuts;

        /// <summary>
        /// Creates a Fuzzy number from a list of alpha-cuts
        /// </summary>
        /// <param name="alphaCuts">(n-1)th interval in the list must contain (n)th</param>
        public FuzzyNumber(IEnumerable<Interval> alphaCuts)
        {
            this.alphaCuts = alphaCuts.ToArray();

            for (int i = 1; i < this.alphaCuts.Length; i++)
            {
                if (!this.alphaCuts[i - 1].Contains(this.alphaCuts[i]))
                    throw new ArgumentException("Fuzzy number expects a list of intervals where (n-1)th interval contains (n)th.");
            }

            if (this.alphaCuts.Length == 0)
                throw new ArgumentException("Fuzzy number expects at least one interval.");
        }

        private ReadOnlyCollection<Interval> alphaCutsWrapper; // read-only alpha-cuts wrapper

        /// <summary>
        /// List of alpha-cuts
        /// </summary>
        public ReadOnlyCollection<Interval> AlphaCuts
        {
            get {
                if (alphaCutsWrapper == null)
                    alphaCutsWrapper = Array.AsReadOnly(alphaCuts);

                return alphaCutsWrapper;
            }
        }

        /// <summary>
        /// Alpha-cut with membership degree 1
        /// </summary>
        public Interval Top
        {
            get { return alphaCuts[alphaCuts.Length - 1]; }
        }

        /// <summary>
        /// Alpha-cut with membership degree 0
        /// </summary>
        public Interval Bottom
        {
            get { return alphaCuts[0]; }
        }

        /// <summary>
        /// Calculates alpha-cut by its membership
        /// </summary>
        /// <param name="alpha">Membership degree</param>
        /// <returns>An alpha-cut at specified membership degree</returns>
        public Interval GetAlphaCut(double alpha)
        {
            if (alpha > 1 || alpha < 0)
                throw new ArgumentOutOfRangeException("Alpha value", "Alpha value must be in the interval [0, 1].");

            double i = alpha * (alphaCuts.Length - 1);
            if (i % 1 == 0)
                return alphaCuts[(int)i];

            // alpha-cut is not in the list and must be interpolated
            int upper = (int)Math.Ceiling(i);
            int lower = (int)Math.Floor(i);

            double ratio = upper - i;
            double a, b;

            if (alphaCuts[lower].A == alphaCuts[upper].A)
                a = alphaCuts[lower].A;
            else
                a = alphaCuts[upper].A - ratio * (alphaCuts[upper].A - alphaCuts[lower].A);

            if (alphaCuts[lower].B == alphaCuts[upper].B)
                b = alphaCuts[lower].B;
            else
                b = alphaCuts[upper].B + ratio * (alphaCuts[lower].B - alphaCuts[upper].B);

            return new Interval(a, b);
        }

        /// <summary>
        /// Calculates membership degree
        /// </summary>
        /// <param name="value">Member of the fuzzy set (number)</param>
        /// <returns>Degree of membership</returns>
        public double GetMembership(double value)
        {
            if (Top.Contains(value))
                return 1;

            if (!Bottom.Contains(value))
                return 0;

            for (int i = 1; i < alphaCuts.Length; i++)
            {
                if (!alphaCuts[i].Contains(value))
                {
                    if (alphaCuts[i].B < value)
                        return ((i - 1) + (alphaCuts[i - 1].B - value) / (alphaCuts[i - 1].B - alphaCuts[i].B)) / (alphaCuts.Length - 1);
                    else
                        return ((i - 1) + (value - alphaCuts[i - 1].A) / (alphaCuts[i].A - alphaCuts[i - 1].A)) / (alphaCuts.Length - 1);
                }
            }

            return 0;
        }

        /// <summary>
        /// Generic unary operation over a Fuzzy Number
        /// </summary>
        /// <param name="number"></param>
        /// <param name="operation">Interval arithmetic unary operation</param>
        /// <returns></returns>
        public static FuzzyNumber ExecuteUnary(FuzzyNumber number, Func<Interval, Interval> operation)
        {
            return new FuzzyNumber(number.AlphaCuts.Select(operation));
        }

        /// <summary>
        /// Generic binary operation over a Fuzzy Number
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// /// <param name="operation">Interval arithmetic binary operation</param>
        /// <returns></returns>
        public static FuzzyNumber ExecuteBinary(FuzzyNumber left, FuzzyNumber right, Func<Interval, Interval, Interval> operation)
        {
            if (left.AlphaCuts.Count == right.AlphaCuts.Count)
                return new FuzzyNumber(left.AlphaCuts.Zip(right.AlphaCuts, operation));

            throw new NotImplementedException();
        }

        public bool Equals(FuzzyNumber other)
        {
            if (other == null)
                return false;

            if (alphaCuts.Length == other.AlphaCuts.Count)
            {
                for (int i = 0; i < alphaCuts.Length; i++)
                {
                    if (alphaCuts[i] != other.AlphaCuts[i])
                        return false;
                }
            }
            else
                throw new NotImplementedException();

            return true;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as FuzzyNumber);
        }

        public override int GetHashCode()
        {
            return alphaCuts.GetHashCode();
        }

        /// <summary>
        /// Concatenates alpha-cuts of the Fuzzy Number into a string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("[{0}]", String.Join(", ", alphaCuts));
        }

    }
}
