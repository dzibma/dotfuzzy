﻿namespace DotFuzzy
{
    public partial class FuzzyNumber
    {

        public static FuzzyNumber operator +(FuzzyNumber left, FuzzyNumber right)
        {
            return ExecuteBinary(left, right, (x, y) => x + y);
        }

        public static FuzzyNumber operator +(FuzzyNumber left, double right)
        {
            return ExecuteUnary(left, x => x + right);
        }

        public static FuzzyNumber operator +(double left, FuzzyNumber right)
        {
            return right + left;
        }

        public static FuzzyNumber operator -(FuzzyNumber left, FuzzyNumber right)
        {
            return ExecuteBinary(left, right, (x, y) => x - y);
        }

        public static FuzzyNumber operator -(FuzzyNumber left, double right)
        {
            return ExecuteUnary(left, x => x - right);
        }

        public static FuzzyNumber operator -(double left, FuzzyNumber right)
        {
            return ExecuteUnary(right, x => left - x);
        }

        public static FuzzyNumber operator *(FuzzyNumber left, FuzzyNumber right)
        {
            return ExecuteBinary(left, right, (x, y) => x * y);
        }

        public static FuzzyNumber operator *(FuzzyNumber left, double right)
        {
            return ExecuteUnary(left, x => x * right);
        }

        public static FuzzyNumber operator *(double left, FuzzyNumber right)
        {
            return right * left;
        }

        public static FuzzyNumber operator /(FuzzyNumber left, FuzzyNumber right)
        {
            return ExecuteBinary(left, right, (x, y) => x / y);
        }

        public static FuzzyNumber operator /(FuzzyNumber left, double right)
        {
            return ExecuteUnary(left, x => x / right);
        }

        public static FuzzyNumber operator /(double left, FuzzyNumber right)
        {
            return ExecuteUnary(right, x => left / x);
        }

        public static bool operator ==(FuzzyNumber left, FuzzyNumber right)
        {
            return ReferenceEquals(left, null) ? ReferenceEquals(right, null) : left.Equals(right);
        }

        public static bool operator !=(FuzzyNumber left, FuzzyNumber right)
        {
            return !(left == right);
        }

    }
}
