﻿using System;
using System.Globalization;

namespace DotFuzzy
{
    /// <summary>
    /// Closed set of reals
    /// </summary>
    public partial struct Interval : IEquatable<Interval>
    {

        private static int decimals = 4; // default number of decimals
        
        /// <summary>
        /// Decimal precision of the boundaries
        /// </summary>
        public static int Decimals
        {
            get {
                return decimals;
            }

            internal set {
                decimals = value;
            }
        }

        private double a, b;

        /// <summary>
        /// Creates a closed interval [a, b]
        /// </summary>
        /// <param name="a">Lower bound</param>
        /// <param name="b">Upper bound</param>
        public Interval(double a, double b)
        {
            if (a > b)
                throw new ArgumentException("An interval [a, b] expects: a <= b.");

            this.a = Math.Round(a, decimals);
            this.b = Math.Round(b, decimals);
        }

        /// <summary>
        /// Creates a degenerate interval [A, B]: a = b
        /// </summary>
        /// <param name="value"></param>
        public Interval(double value)
        {
            a = b = Math.Round(value, decimals);
        }

        /// <summary>
        /// Lower bound
        /// </summary>
        public double A 
        {
            get { return a; } 
        }

        /// <summary>
        /// Upper bound
        /// </summary>
        public double B
        {
            get { return b; }
        }


        /// <summary>
        /// Diameter of the interval
        /// </summary>
        public double Width
        {
            get { return b - a; }
        }

        /// <summary>
        /// Middle point between a and b
        /// </summary>
        public double Midpoint
        {
            get { return a + Width / 2; }
        }

        /// <summary>
        /// Containts a given number?
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Contains(double value)
        {
            return a <= value && value <= b;
        }

        /// <summary>
        /// Containts a given Interval?
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Contains(Interval other)
        {
            return Contains(other.A) && Contains(other.B);
        }

        /// <summary>
        /// Intersects with a given Interval?
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool IntersectsWith(Interval other)
        {
            if (a >= other.A && a <= other.B)
                return true;

            if (a <= other.A && b >= other.A)
                return true;

            return false;
        }

        /// <summary>
        /// Are both intervals equals?
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Interval other)
        {
            return other.A == a && other.B == b;
        }

        public override bool Equals(object obj)
        {
            return obj is Interval ? Equals((Interval)obj) : false;
        }

        /// <summary>
        /// Unique hash of the instance.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return a.GetHashCode() ^ b.GetHashCode();
        }

        /// <summary>
        /// Converts the interval to a string in the form of [a, b].
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("[{0}, {1}]", a.ToString(CultureInfo.InvariantCulture), b.ToString(CultureInfo.InvariantCulture));
        }

    }
}
