﻿namespace DotFuzzy
{
    /// <summary>
    /// Piecewise linear Fuzzy numbers factory
    /// </summary>
    public class FuzzyNumberFactory
    {
        private int levels; // number of alpha-cuts

        public FuzzyNumberFactory(int levels)
        {
            if (levels < 2)
                levels = 2;

            this.levels = levels;
        }

        public FuzzyNumberFactory() : this(11) { }

        /// <summary>
        /// Creates trapezoidal Fuzzy number with bottom alpha-cut [a, d] and top [b, c]
        /// </summary>
        /// <param name="a">[a, d] lower bound</param>
        /// <param name="b">[b, c] lower bound</param>
        /// <param name="c">[b, c] upper bound</param>
        /// <param name="d">[a, d] upper bound</param>
        /// <returns></returns>
        public FuzzyNumber CreateTrapezoid(double a, double b, double c, double d)
        {
            Interval[] alphaCuts = new Interval[levels];

            for (int i = 0; i < alphaCuts.Length; i++)
            {
                alphaCuts[i] = new Interval(
                    a + (b - a) * i / (alphaCuts.Length - 1),
                    d - (d - c) * i / (alphaCuts.Length - 1));
            }

            return new FuzzyNumber(alphaCuts);
        }

        /// <summary>
        /// Creates triangular Fuzzy number with bottom alpha-cut [a, c] and top [b, b]
        /// </summary>
        /// <param name="a">[a, c] lower bound</param>
        /// <param name="b">[b, b] bounds</param>
        /// <param name="c">[a, c] upper bound</param>
        /// <returns></returns>
        public FuzzyNumber CreateTriangle(double a, double b, double c)
        {
            return CreateTrapezoid(a, b, b, c);
        }

        /// <summary>
        /// Alias for the <see cref="CreateTriangle"/> method
        /// </summary>
        public FuzzyNumber CreateTFN(double a, double b, double c)
        {
            return CreateTriangle(a, b, c);
        }

        /// <summary>
        /// Creates crisp Fuzzy number
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public FuzzyNumber CreatePoint(double a)
        {
            return CreateTriangle(a, a, a);
        }
    }
}
